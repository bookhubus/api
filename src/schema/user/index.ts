import { Context } from '../../lib'
import { merge } from 'lodash';
import mutations from './mutations';

export { default as typeDef } from './schema'

export const resolvers = merge({
    Query: {
        me: async (parent, args, { models, user }: Context) => {
            const userFromDb = await models._collections.Users.findOne({});
            
            return {
                id: userFromDb._id.toString(),
                name: userFromDb.name
            }
        }
    }
}, mutations)

