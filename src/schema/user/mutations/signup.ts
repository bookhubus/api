import { ObjectID } from 'mongodb';
import { Context } from '../../../lib'
import { MutationSignupArgs, SignupInput, AuthProviderInput, AuthProviders, SignupPayload, User } from '../../../generated/graphql'
import { generatePasswordHash, encodeJWT } from '../../../helpers/hash'
import yup from '../../../helpers/validation';


export default {
    Mutation: {
        signup: async (parent, { input }: MutationSignupArgs, context: Context): Promise<SignupPayload> => {
            const { models, user, config } = context;

            const validationSchema = yup.object<SignupInput>({
                name: yup.string().required().min(2).max(30),
                username: yup.string().required().min(2).max(30).usernameNotUsed(),
                authProvider: yup.object<AuthProviderInput>({
                    provider: yup.mixed<AuthProviders>(),

                    // When the auth provider is email
                    email: yup.string().when('provider', {
                        is: (provider) => provider == "EMAIL",
                        then: yup.string().required().min(2).email().emailNotUsed(),
                        otherwise: yup.string().notRequired()
                    }),

                    password: yup.string(),

                })
            }).defined()

            await validationSchema.validateAndThrow(input, { context });


            let newUser = {
                _id: new ObjectID(),
                name: input.name,
                username: input.username,
                email: input.authProvider.email,
                bio: null,
                image: "https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
                roles: ["user"],
                linkedAccounts: [
                    {
                        provider: 'email',
                        email: input.authProvider.email,
                        password: await generatePasswordHash(input.authProvider.password),
                    },
                ],
            }

            const response = await models._collections.Users.insertOne({
                ...newUser,
                createdAt: new Date(),
                updatedAt: new Date(),
            })

            return {
                user: {
                    id: newUser._id.toString(),
                    name: newUser.name,
                    username: newUser.username,
                    email: newUser.email,
                    image: newUser.image,
                    bio: newUser.bio,
                    isMe: true,
                },
                accessToken: encodeJWT(config, {
                    id: newUser._id.toString(),
                    name: newUser.name,
                    roles: newUser.roles,
                }),
            }
        }

    }
}

