import { gql } from 'apollo-server'

export default gql`

extend type Query {
	me: User
	user(id: String!): User
	accessToken(refreshToken: String!): String
}

extend type Mutation {
	signup(input: SignupInput!): SignupPayload!
	signinUser(input: SigninInput!): SigninPayload!
	requestResetPasswordLink(input: RequestResetPasswordLinkInput!):  RequestResetPasswordLinkPayload

	updateUser(
		id: String!
		name: String
		email: String
		password: String
		age: String
		gender: String
		bios: String
		image: String
		authProviders: [AuthProviderInput!]
	): User!
	deleteUser(input: DeleteUserInput!): DeleteUserPayload!
}

enum AuthProviders {
    EMAIL
    GOOGLE
}

input AuthProviderInput {
	provider: AuthProviders!
	# IF Email Provider
	email: String!
	password: String!
}

input SignupInput {
	name: String!
	username: String!
    authProvider: AuthProviderInput!
}

type SignupPayload {
    user: User!
    accessToken: String!
}

input SigninInput {
    authProvider: AuthProviderInput!
}

type SigninPayload {
    user: User!
    accessToken: String!
}


input RequestResetPasswordLinkInput {
    email: String!
}

type RequestResetPasswordLinkPayload {
    status: Boolean!
}


input DeleteUserInput {
    password: String!
}

type DeleteUserPayload {
    status: Boolean!
}

type User {
    id: ID!
    name: String!
    username: String!
    email: String!
    bio: String
    image: String!
    isMe: Boolean!
}

`
