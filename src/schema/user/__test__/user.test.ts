import { reset } from '../../../db-utils/lib'
import { gql } from 'apollo-server';

import { GetConfig } from '../../../config';
import connectDB, { DBContext } from '../../../db';
import createTestClient from '../../../tets-utils/TestClass';

const config = GetConfig('Test')

let db: DBContext = null;

beforeAll(async () => {
    db = await connectDB(config)
});


afterAll(async () => {
    db._client.close()
});

beforeEach(async () => {
    await reset(config, db);
});

test('user should work', async () => {

    const { query } = createTestClient({ config: config },
        {
            req: {
                headers: {
                    // authorization: `JWT ${token}`
                }
            },
        });

    const response = await query({
        query: gql`
        {
            me {
                id
                name
            }
        }

        `,

    })

    expect(response.data).toMatchSnapshot();
});
