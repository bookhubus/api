import { merge } from 'lodash';
import { makeExecutableSchema, gql } from 'apollo-server'
import {
  typeDef as usertypeDef,
  resolvers as userResolvers,
} from './user';


// Construct a schema, using GraphQL schema language
const typeDefs = gql`
  type Query {
    hello: String
    info: SystemInfo!
  }

  type Mutation {
    hello: String
  }

  type SystemInfo {
    environment: String
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    hello: () => 'Hello world!',
    info: (parent, args, { config }) => ({
      environment: config.Name
    }),
  },
  Mutation: {
    hello: () => 'Hello world!',
  },
};

export default makeExecutableSchema({
  typeDefs: [typeDefs, usertypeDef],
  resolvers: merge(resolvers, userResolvers),
});