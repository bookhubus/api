const { ApolloServer } = require("apollo-server");
const { createTestClient } = require("apollo-server-testing");
import createApolloServerConfig from '../lib'

export default (config, ctxArg) => {
    const baseCtxArg = ctxArg;
    let currentCtxArg = baseCtxArg;

    const apolloConfig : any = createApolloServerConfig(config);

    const { query, mutate, ...others } = createTestClient(
        new ApolloServer({
            ...apolloConfig,
            context: () => apolloConfig.context(currentCtxArg),
        })
    );

    // Wraps query and mutate function to set context arguments
    const wrap = fn => ({ ctxArg, ...args }) => {
        currentCtxArg = ctxArg != null ? ctxArg : baseCtxArg;
        return fn(args);
    };

    return { query: wrap(query), mutate: wrap(mutate), ...others };
};