// Could be improved to get the configuration from and secrets manger like azure keyvault
const ENVIRONMENTS = {
    Local: {
        Name: "Local",
        DATABASE_URL: "mongodb://localhost:27017/bookhub",
        DATABASE_NAME: "bookhub",
        JWT_SECRET: "secret"
    },
    Test: {
        Name: "Test",
        DATABASE_URL: "mongodb://localhost:27017/bookhub-test",
        DATABASE_NAME: "bookhub-test",
        JWT_SECRET: "secret"
    },
    Development: {
        Name: "Development",
        DATABASE_URL: "mongodb+srv://admin:123456as@cluster0.mslsb.mongodb.net/dev?retryWrites=true&w=majority",
        DATABASE_NAME: "dev",
        JWT_SECRET: "secret"
    },
    Staging: {
        Name: "Staging",
        DATABASE_URL: "mongodb+srv://admin:123456as@cluster0.mslsb.mongodb.net/stg?retryWrites=true&w=majority",
        DATABASE_NAME: "stg",
        JWT_SECRET: "secret"
    },
    Production: {
        Name: "Production",
        DATABASE_URL: "mongodb+srv://admin:123456as@cluster0.mslsb.mongodb.net/prod?retryWrites=true&w=majority",
        DATABASE_NAME: "prod",
        JWT_SECRET: "secret"
    }
};


const BranchToEnvironmentName = (branchName) => {
    switch (branchName) {
        case "dev":
            return "Development"
        case "stg":
            return "Staging"
        case "master":
            return "Production"
        case undefined:
            return null
        default:
            return "Development"
    }
}


export const GetConfig = (environment?) => {
    let config: IConfig;
    if (environment) {
        config = ENVIRONMENTS[environment]
    } else {
        config = ENVIRONMENTS[process.env.ACTIVE_ENVIRONMENT || BranchToEnvironmentName(process.env.VERCEL_GITLAB_COMMIT_REF) || 'Local']
    }

    return { ...config, ...process.env }
}

export default GetConfig()


export interface IConfig {
    Name: string,
    DATABASE_URL: string,
    DATABASE_NAME: string,
    JWT_SECRET: string
}