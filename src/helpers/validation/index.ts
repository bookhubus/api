import * as yup from 'yup';
import validate from './ValidationHelper'
import addCustomValidators from './validators';


yup.mixed.prototype['validateAndThrow'] = validate;
declare module "yup" {
    interface Schema<T> {
        validateAndThrow(value: T, options?: ValidateOptions): Promise<T>;
    }
}

addCustomValidators(yup)

export default yup;
