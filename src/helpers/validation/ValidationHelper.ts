import { UserInputError } from 'apollo-server'
import { ValidateOptions } from 'yup';

// Generic form error class.  Allows simple error detection with .is(), and
// an ErrorType-compatible object with .errors()
export default async function(value: any, options?: ValidateOptions) {

	await this.validate(value, { abortEarly: false, ...options }).catch((err) => {
		const errors = err.inner.map(e => ({
			path: e.path,
			code: `${e.path}_${e.type}`,
			message: e.errors[0]
		}));

		throw new UserInputError(
			'Failed to get events due to validation errors',
			{
				validationErrors: errors,
			}
		)
	});

}
