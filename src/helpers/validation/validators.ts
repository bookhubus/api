import { Context } from '../../lib'

declare module "yup" {
    interface StringLocale {
        emailNotUsed?: TestOptionsMessage;
    }

    interface StringSchema<T> {
        emailNotUsed(message?: StringLocale['emailNotUsed']): StringSchema<T>;
        usernameNotUsed(message?: StringLocale['emailNotUsed']): StringSchema<T>;
    }
}

export const addEmailValidator = (yup) => {
    yup.addMethod(yup.string, 'emailNotUsed', function() {
        return this.test(
            'emailNotUsed',
            '${path} is not available',
            async function (value) {
                const { options: { context }, createError }: { options: { context: Context }, createError: any } = this;

                const numberOfUsers = await context.models._collections.Users.countDocuments({
                    linkedAccounts: {
                        $elemMatch: { provider: 'email', email: value },
                    },
                })

                return numberOfUsers == 0;
            }
        );
    });
}


export const addUsernameValidator = (yup) => {
    yup.addMethod(yup.string, 'usernameNotUsed', function() {
        return this.test(
            'usernameNotUsed',
            '${path} is not available',
            async function (value) {
                const { options: { context } }: { options: { context: Context } } = this;

                const numberOfUsers = await context.models._collections.Users.countDocuments({
                    username: value,
                })

                return numberOfUsers == 0;
            }
        );
    });
}

export default (yup) => {
    addEmailValidator(yup);
    addUsernameValidator(yup);
}