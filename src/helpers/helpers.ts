
export const getJWT = (authorization) => {
	const temp = authorization.split(' ')
	if (temp.length == 2 && temp[0].toLowerCase() == 'bearer') {
		return temp[1]
	} else {
		return null
	}
}
