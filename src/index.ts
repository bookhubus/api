import { ApolloServer } from 'apollo-server'

import createApolloServerConfig from './lib';
import config from './config';


const server = new ApolloServer(createApolloServerConfig({ config }))
// The `listen` method launches a web server.
const port = process.env.PORT || 3000;
server.listen(port).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});