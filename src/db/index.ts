import { MongoClient, Collection } from 'mongodb'


export interface RawCollections {
	Users: Collection
}

export interface DBContext {
	_client: MongoClient,
    _collections: RawCollections
}

let cachedDBModel = null

export default async (config) : Promise<DBContext> => {

    if (cachedDBModel != null) {
        return cachedDBModel;
    }
    
	const client = await MongoClient.connect(
		config.DATABASE_URL,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	const db = client.db(config.DATABASE_NAME)
	const collections = {
		Users: db.collection('Users'),
	}


	cachedDBModel = {
		_client: client,
		_collections: collections,
    }
    
    return cachedDBModel;
}
