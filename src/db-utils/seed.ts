import { seed } from './lib'
import config from '../config';

const start = async () => {
  await seed(config)
}

start()