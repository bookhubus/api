import { DBContext } from "../db"
import { Seeder } from 'mongo-seeding'
import data from "./data";

export const seed = async (config) : Promise<void> => {
    const seederConfig = {
        database: config.DATABASE_URL,
    };
    const seeder = new Seeder(seederConfig);

    try {
        await seeder.import(data);
    }
    catch(err) {
    }
}

export const emptyDatabase = async (db: DBContext) => {
    await Promise.all(Object.values(db._collections).map(async (c) => {
        await c.deleteMany({})
    }))
}

export const reset = async (config, db: DBContext) => {
    await emptyDatabase(db)
    await seed(config)
}
