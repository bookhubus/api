import connectDB from '../db'

import { reset } from './lib'
import config from '../config';

const start = async () => {
  const db = await connectDB(config)
  await reset(config, db)
  await db._client.close();
}

start()