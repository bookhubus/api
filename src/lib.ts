import { Config } from 'apollo-server'
import schema from './schema'
import { getJWT } from './helpers/helpers'
import { decodeJWT } from './helpers/hash'
import connectDB, { DBContext } from './db'
import { IConfig } from './config'


export interface IContextUser {
  isAuthenticated: boolean,
  accessToken: string,
  [key: string]: any
}

export interface Context {
  config: IConfig,
  models: DBContext,
  user: IContextUser
}


export default ({ config }): Config => {

  const buildContext = async ({ req }) : Promise<Context> => {
    const authHeader = req.headers.authorization || req.headers.Authorization
    const jwt = authHeader ? getJWT(authHeader) : null
    const payload = jwt ? decodeJWT(config, jwt) : null
    const user = payload
      ? Object.assign({ isAuthenticated: true }, payload, { accessToken: jwt })
      : { isAuthenticated: false }

    const db = await connectDB(config)

    return { config, models: db, user }
  }


  return {
    schema,
    context: buildContext,
    // mocks: true,
    introspection: true,
    playground: true
  }
}
