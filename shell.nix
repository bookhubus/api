{ pkgs ? import <nixpkgs> {} }:

let
in

pkgs.mkShell {

    # Installed Packages
    buildInputs = [ 
        pkgs.nodejs-13_x
    ];

    # Enviroment Variables
    DOCKER_BUILDKIT = 1;
}
