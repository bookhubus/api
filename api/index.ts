import { ApolloServer } from 'apollo-server-micro'
import createApolloServerConfig from '../src/lib';
import config from '../src/config';


const server = new ApolloServer(createApolloServerConfig({ config }))

export default server.createHandler({
  path: '/'
});